from heapq import heappush, heappop
import sys

from Utils import clock, set_clock, register_event, get_event, update_statistics, statistics_compile
import TruckingModule as trucking

if __name__ == "__main__":
    ''' oracle system '''
    # prime initial event
    register_event(trucking.TruckArrival())
    stats_target = 2880 #end of day 2
    # main loop
    while clock() <= 8_640: # run for 6 days (1 warmup + 5 stats collection)
        event = get_event()
        #TODO print(f'{clock()} -> {event.__class__.__name__}')
        set_clock(event.event_time())
        update_statistics()
        if clock() >= stats_target:
            sys.stderr.write(f'day {stats_target//1440} stats -> {statistics_compile()}\n')
            stats_target += 1440
        event.handle()
    # other stats written to stderr
    # this allows us to use stdout to isolate output of final day.
    print(f"{statistics_compile()}")