import subprocess, threading, math

import numpy, scipy.special

PYTHON_COMMAND = 'python3'
TEST_COUNT = 64
CERTAINTY = 0.95

results = []

class Runner(threading.Thread):
    def run(self):
        ret = subprocess.run([PYTHON_COMMAND, 'Simulator.py'], capture_output=True, text=True)
        results.append(ret.stdout)

# run tests
runners = [Runner() for _ in range(TEST_COUNT)]
print(f'running {TEST_COUNT} tests...')
for runner in runners: runner.start()
for runner in runners: runner.join()

# tests done, produce final statistics
print('processing results...')
results = list(map(lambda x: eval(x), results))
avgs = {key: sum([result[key] for result in results]) / len(results) for key in results[0]}
variances = {key: sum([(result[key] - avgs[key]) ** 2 for result in results]) / (len(results) - 1) for key in avgs.keys()}
half_alpha_certainty = 1 - ((1 - CERTAINTY) / 2)
half_widths = {key: scipy.special.stdtrit(TEST_COUNT - 1, half_alpha_certainty) * math.sqrt(variance / TEST_COUNT) for key, variance in variances.items()}
print('')
for key in avgs.keys():
    print(f"{key: ^30}->\t{round(avgs[key],3):,} +/- {round(half_widths[key],5)}")