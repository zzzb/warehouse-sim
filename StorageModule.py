import numpy

import DispatchModule as dispatch
from Utils import Event, Bag, clock, register_event, lock, nspd, statistic_add, statistic_max, register_statistics_updater

# release schedule of 5 PPM between 2p and 8p
schedule_std = [0 for _ in range(840)]+[5 for _ in range(360)]+[0 for _ in range(240)]
# release schedule of 5 PPM between noon and 10pm
# note that expedited package release MUST encompass
# all release times that standard packages do.
# this is effectively an override schedule for when we have
# expedited packages waiting.
schedule_exp = [0 for _ in range(720)]+[5 for _ in range(600)]+[0 for _ in range(120)]

queue_std = []
queue_exp = []

handler = [None for _ in range(35)]

def stat_update(delta:float):
    # average handlers in use
    statistic_add('storage.handlers.avg', (len(handler) - handler.count(None)) * delta)
    # average bags stored by category
    statistic_add('storage.expedited.avg', len(queue_exp) * delta)
    statistic_add('storage.standard.avg', len(queue_std) * delta)
    # max bags stored by category
    statistic_max('storage.held.max', len(queue_exp) + len(queue_std))
    # max age of bag by category
    if len(queue_exp) > 0:
        statistic_max('storage.expedited_age.max', max(map(lambda x: clock() - x.filled_time, queue_exp)))
    if len(queue_std) > 0:
        statistic_max('storage.standard_age.max', max(map(lambda x: clock() - x.filled_time, queue_std)))
register_statistics_updater(stat_update)

def accept_bag(bag:Bag):
    ''' API - move bag into storage '''
    (queue_exp if bag.expedited else queue_std).append(bag)
    if handler.count(None) > 0:
        # locking with a bag solely because it is a thing to put in there
        # there is no significance to it otherwise
        register_event(StoreEvent(lock(handler, bag)))

class StoreEvent(Event):
    ''' decides a time when a package should be queued to be emitted '''
    def __init__(self, handlerno):
        self.handlerno = handlerno
        Event.__init__(self)

    def compute_event_delay(self):
        return nspd(schedule_exp if len(queue_exp) > 0 else schedule_std, clock())

    def handle(self):
        if len(queue_exp) > 0:
            bag = queue_exp.pop(0)
            if not dispatch.accept_bag(bag):
                queue_exp.insert(0, bag)
            register_event(StoreEvent(self.handlerno))
        elif len(queue_std) > 0 and schedule_std[int(clock()) % len(schedule_std)] != 0:
            # schedule_std term ensures that we do not deliver standard packages outside their hours
            bag = queue_std.pop(0)
            if not dispatch.accept_bag(bag):
                queue_std.insert(0, bag)
            register_event(StoreEvent(self.handlerno))
        else:
            # no packages to emit, terminate worker loop to save cycles
            handler[self.handlerno] = None