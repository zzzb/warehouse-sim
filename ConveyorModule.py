import random, numpy

from Utils import Event, Package, Bag, register_event, clock, lock, statistic_add, statistic_max, register_statistics_updater
import StorageModule as storage

#
# Describes the conveyor belt
#

resorts = []
''' pool of elements to be resorted '''
resorts_load = []

labeler_queue = [] # packages that need labeling
labelers = [None for _ in range(55)] # we have 55 people doing labeling

region_queue = [[] for _ in Package.regions]
''' contains lists of (in_range_time, package) '''
region_sorters = [[None for _ in range(21)] for _ in Package.regions]
region_delay = [30/60,45/60, 60/60] # time to reach regional sorting in minutes

bags = [[Bag(region, route, False) for route in Package.routes] for region in Package.regions]
''' sorted van-loads of packages destined for a specific route '''
bags_expedited = [[Bag(region, route, True) for route in Package.routes] for region in Package.regions]
''' [expedited] sorted van-loads of packages destined for a specific route '''

# ensure that our vars match the size of Package's 
assert len(region_queue) == len(region_sorters)
assert sum(map(lambda x: len(x), bags)) == len(Package.regions) * len(Package.routes)

def stats_update(delta:float):
    # average packages in labeling system
    in_label = len(labelers) - labelers.count(None) + len(labeler_queue)
    statistic_add('conveyor.in_labeling.avg', in_label * delta)
    statistic_max('conveyor.in_labeling.max', in_label)
    # average packages in conveyor system but not yet fully sorted
    in_sort = sum(map(lambda x: len(x) - x.count(None), region_sorters)) + sum(map(len, region_queue))
    statistic_add('conveyor.in_sorting.avg', in_sort * delta)
    statistic_max('conveyor.in_sorting.max', in_sort)
# register stats handler
register_statistics_updater(stats_update)

def accept_package(package:Package):
    ''' API - package goes to labeling '''
    # incoming package count includes packages that failed to be delivered
    statistic_add('conveyor.incoming.cnt', 1)
    if labelers.count(None) > 0:
        register_event(LabelingEvent(lock(labelers, package)))
    else:
        labeler_queue.append(package)

class LabelingEvent(Event):
    ''' event representing a package being labeled and moved onto the conveyor belt '''

    def __init__(self, labeler_no):
        Event.__init__(self)
        self.labeler_no = labeler_no

    def compute_event_delay(self):
        return numpy.random.exponential(10/60) # labeling takes 10 seconds
    
    def handle(self):
        #store the package
        package = labelers[self.labeler_no]
        # remove package from labeler
        if len(labeler_queue) > 0:
            # start labeling next package
            labelers[self.labeler_no] = labeler_queue.pop(0)
            register_event(LabelingEvent(self.labeler_no))
        else:
            # free the labeler
            labelers[self.labeler_no] = None
        # place package on conveyor belt
        register_event(TravelBeltEvent(package))


class TravelBeltEvent(Event):
    ''' event representing a package moving along the conveyor belt '''

    def __init__(self, package):
        self.package = package
        Event.__init__(self)

    def compute_event_delay(self):
        # delay determined by the stop it needs to reach
        return numpy.random.exponential(region_delay[self.package.region])

    def handle(self):
        # register the package with the sorter for its region
        region = self.package.region
        sorters = region_sorters[region]
        if sorters.count(None) > 0:
            register_event(SortPackageEvent(region, lock(sorters, self.package)))
        else:
            region_queue[region].append((clock(), self.package))

class SortPackageEvent(Event):
    ''' event representing a package being sorted onto a cart '''

    def __init__(self, region, sorter_no):
        self.region = region
        self.sorter_no = sorter_no
        Event.__init__(self)

    def compute_event_delay(self):
        return numpy.random.exponential(10/60) #sorting takes 10 seconds

    def handle(self):
        package = region_sorters[self.region][self.sorter_no]
        # add the next package to the sorter
        # the package passes us after 5 seconds, so send to resort instead if this happens
        while len(region_queue[self.region]) > 0 and clock() - region_queue[self.region][0][0] < 5/60:
            expired_package = region_queue[self.region].pop(0)[1]
            register_event(ToResortEvent(expired_package))
        if len(region_queue[self.region]) > 0:
            # shift next package into sorter slot & register new event
            region_sorters[self.region][self.sorter_no] = region_queue[self.region].pop(0)[1]
            register_event(SortPackageEvent(self.region, self.sorter_no))
        else:
            # no next event, release sorter
            region_sorters[self.region][self.sorter_no] = None
        # send package to route bagging
        register_event(BaggingEvent(package))

class BaggingEvent(Event):
    '''
    event representing a package being sorted into a bag
    terminal event for a package within the conveyor system
    this event changes between U and L models
    '''
    def __init__(self, package):
        self.package = package
        Event.__init__(self)
    
    def compute_event_delay(self):
        return numpy.random.exponential(5/60) # 5 seconds to bag the package

    def handle(self):
        bag = bags_expedited[self.package.region][self.package.route] if self.package.expedited else bags[self.package.region][self.package.route]
        bag.add(self.package)
        if bag.is_full():
            # replace the bag being filled by the conveyor
            (bags_expedited if bag.expedited else bags)[bag.region][bag.route] = Bag(bag.region, bag.route, bag.expedited)
            # send the bag to storage
            storage.accept_bag(bag)

class ToResortEvent(Event):
    ''' event representing a package moving into the resort pile '''

    def __init__(self, package):
        statistic_add('conveyor.resorted.cnt', 1)
        self.package = package
        Event.__init__(self)

    def compute_event_delay(self):
        return numpy.random.exponential(max(region_delay) - region_delay[self.package.region] + 15/60)

    def handle(self):
        global resorts
        # pass value into resorts
        resorts.append(self.package)
        if len(resorts) > 100:
            # resort queue filled, cart back to start of belt
            register_event(ResortEvent(resorts))
            resorts = []

class ResortEvent(Event):
    ''' event representing the resort pile being dumped back onto the conveyor '''
    def __init__(self, resort_load):
        self.resort_load = resort_load
        Event.__init__(self)

    def compute_event_delay(self):
        return numpy.random.exponential(2) # 2 minutes to cart packages back to start of conveyor

    def handle(self):
        # place packages back onto belt
        register_event(ResortUnloadEvent(self.resort_load))

class ResortUnloadEvent(Event):
    ''' event representing a package being unloaded from a resort cartload '''

    def __init__(self, resort_load):
        self.resort_load = resort_load
        Event.__init__(self)

    def compute_event_delay(self):
        return numpy.random.exponential(2/60) # packages take 2 seconds to load back onto belt

    def handle(self):
        package = self.resort_load.pop(0)
        # eject package back onto conveyor - do not re-label
        register_event(TravelBeltEvent(package))
        if len(self.resort_load) > 0:
            register_event(ResortUnloadEvent(self.resort_load))