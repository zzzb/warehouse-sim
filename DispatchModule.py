import numpy
from typing import List

from Utils import Event, Bag, clock, Package, register_event, lock, statistic_add, register_statistics_updater
import ConveyorModule as conveyor

vans:List[Bag] = [None for _ in range(40)] # 40 vans available
''' vans ready to dispatch packages '''

def stats_update(delta:float):
    # average vans deployed
    statistic_add('dispatch.deployed.avg', (len(vans) - vans.count(None)) * delta)
register_statistics_updater(stats_update)

def accept_bag(bag: Bag) -> bool:
    ''' load a van if one ready, otherwise return false '''
    if vans.count(None) > 0:
        register_event(VanDispatchEvent(lock(vans, bag)))
        return True
    else:
        return False


class VanDispatchEvent(Event):
    ''' represents a bag being dispatched '''
    def __init__(self, vanno:int):
        self.vanno = vanno
        bag = vans[vanno]
        self.region = bag.region
        self.route = bag.route
        self.expedited = bag.expedited
        Event.__init__(self)

    def compute_event_delay(self):
        return numpy.random.exponential(1.5)

    def handle(self):
        ''' van returns, possibly with undelivered packages '''
        # each route averages 10 leftover bags
        excess = round(numpy.random.exponential(6))
        # add excess bags back into conveyor
        for _ in range(excess):
            package = Package()
            package.region = self.region
            package.route = self.route
            package.expedited = self.expedited
            conveyor.accept_package(Package())
        # unlock to allow sending next bag
        vans[self.vanno] = None