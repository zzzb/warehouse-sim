# warehouse-sim

OR335 warehouse simulation model for team ```FEAR```

---

to install dependencies, execute:

```pip3 install -r requirements.txt```

---

Run model using:

```python3 Simulator.py```

events will be printed to stdout as they occur.

---

Switch to the L-type model by executing:

```git checkout l-type```

and back to the U-type model by executing:

```git checkout master```