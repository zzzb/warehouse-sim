import numpy, random

import ConveyorModule as conveyor
from Utils import Event, Package, clock, register_event, nspd, lock, statistic_add, statistic_max, register_statistics_updater

#
# handles incoming packages and their unloading onto the conveyor
#

# trucks arrive from 6a-10a, then from 12p-2p
# some trucks arrive in the two hours after each of those
# and very few arrive outside of those ranges
schedule = [1/240 for _ in range(360)]+[1/10 for _ in range(240)]+[1/60 for _ in range(120)]+[1/10 for _ in range(120)]+[1/60 for _ in range(120)]+[1/240 for _ in range(480)]
''' truck arrival schedule '''

truck_queue = []
bays = [None for _ in range(4)]
unloaders = [None for _ in range(30)]

def stats_update(delta:float):
    # average occupied bay count
    statistic_add('trucking.bays_used.avg', (len(bays) - bays.count(None)) * delta)
    # average occupied unloaders
    statistic_add('trucking.unloaders_used.avg', (len(unloaders) - unloaders.count(None)) * delta)
    # average truck_queue
    statistic_add('trucking.queue.avg', len(truck_queue) * delta)
    # max truck queue
    statistic_max('trucking.queue.max', len(truck_queue))
register_statistics_updater(stats_update)

class Truck():
    def __init__(self):
        self.package_count = 2000 # each truck comes in with 100 packages

class TruckArrival(Event):
    def compute_event_delay(self):
        return nspd(schedule, clock()) # trucks arrive on given poisson-schedule
    
    def handle(self):
        ''' handle a truck arriving '''
        truck = Truck()
        # if truck fits, start unloading
        if bays.count(None) != 0 and unloaders.count(None) > 0:
            bayno = lock(bays, truck)
            for i in range(unloaders.count(None)):
                register_event(PackageUnloadEvent(bayno, lock(unloaders, truck)))
        else:
            # truck does not fit, get in line
            truck_queue.append(truck)
        #register next truck arrival
        register_event(TruckArrival())

class PackageUnloadEvent(Event):

    def __init__(self, bayno, unloader):
        self.bayno = bayno
        self.unloader = unloader
        Event.__init__(self)

    def compute_event_delay(self):
        return numpy.random.exponential(20/60) # 10 seconds to unload package

    def handle(self):
        ''' handle package unloading '''
        truck = bays[self.bayno]
        if truck == None:
            # truck was emptied while we were waiting, move to another or terminate
            for i,tr in enumerate(bays):
                if tr != None:
                    self.bayno = i
                    truck = tr
                    break
            else:
                # no trucks to unload, unlock and terminate
                unloaders[self.unloader] = None
                return
        # place package on conveyor belt
        conveyor.accept_package(Package())
        # decrement number of remaining packages to emit
        truck.package_count -= 1
        if truck.package_count > 0:
            # unload the next package
            register_event(PackageUnloadEvent(self.bayno, self.unloader))
        else:
            # done unloading, truck departs
            if len(truck_queue) > 0:
                # move next truck into the bay and begin unloading
                bays[self.bayno] = truck_queue.pop(0)
                # we can start unloading the new truck
                register_event(PackageUnloadEvent(self.bayno, self.unloader))
            else:
                # no next truck, bay stays empty
                bays[self.bayno] = None
                # note that unloader will find a new bay to work on during it's next cycle