import numpy

import random, heapq
from abc import abstractmethod
from typing import Callable

global current_time
global events
current_time = 0
events = []

global statistics, statistics_sources, statistics_last_time
statistics = {}
statistics_updaters = []
statistics_last_time = 0.0
statistics_start_time = 1440

class Event():
    '''
    a generic event class
    all event classes should implement this
    '''
    def __init__(self):
        self.created = clock()
        self.fire_time = self.created + self.compute_event_delay()

    @abstractmethod
    def compute_event_delay(self):
        raise Exception()

    def event_time(self):
        return self.fire_time

    def existed_time(self):
        return sim.clock() - self.created

    def __lt__(self, other:object):
        return self.fire_time < other.fire_time

    @abstractmethod
    def handle(self):
        raise Exception()

class Package():

    regions = [i for i in range(3)]
    ''' the number of regions the package could belong to '''
    routes = [i for i in range(3)]
    ''' the number of routes within each region '''

    def __init__(self):
        ''' construct a package - pre-compute it's destination and expediency '''
        self.region = random.choice(Package.regions)
        self.route = random.choice(Package.routes)
        self.expedited = random.randrange(100) <= 37 # 37% chance of expedited delivery
        self.create_time = clock()

class Bag():

    capacity = 200

    def __init__(self, region:int, route:int, expedited:bool):
        self.contents = 0
        self.region = region
        self.route = route
        self.expedited = expedited
        self.create_time = clock()

    def add(self, package):
        self.contents += 1

    def is_full(self):
        if self.contents == Bag.capacity:
            self.filled_time = clock()
            return True
        else:
            return False

def lock(resources, elem):
    for i,resource in enumerate(resources):
        if resource == None:
            resources[i] = elem
            return i
    raise Exception()

def clock():
    global current_time
    return current_time

def set_clock(time:float):
    global current_time
    current_time = time

def register_event(event:Event):
    heapq.heappush(events, event)

def get_event():
    return heapq.heappop(events)

def nspd(distribution, pos):
    '''
    Modular Non-stationary Poisson Distribution
    Returns a time delta given a distribution and a starting time
    distribution -> array representing the distribution
    pos -> the starting index of the distribution to generate from
    '''
    # get the min beta for the dataset
    maxbeta = max(distribution)
    rate = 1/maxbeta
    delta = 0
    while True:
        # walk one betastep
        delta += numpy.random.exponential(rate)
        if random.random() < distribution[int((pos + delta) % len(distribution))] / maxbeta:
            # value was not thinned!  return it
            return delta

def update_statistics():
    ''' performs all non-event-counted statistic updates '''
    if clock() < statistics_start_time: return
    global statistics_last_time
    delta = clock() - statistics_last_time
    statistics_last_time = clock()
    for updater in statistics_updaters:
        updater(delta)

def register_statistics_updater(updater:Callable[[float],None]):
    ''' registers a new statistics update function to be called by update_statistics() '''
    statistics_updaters.append(updater)

def statistic_add(stat:str, amnt:float):
    ''' 
    adds the given ammount to a statistic
    zero-initializes if needed
    '''
    if clock() < statistics_start_time: return
    if stat not in statistics: statistics[stat] = 0.0
    statistics[stat] += amnt

def statistic_set(stat:str, value:float):
    ''' updates a statistic to the given value '''
    if clock() < statistics_start_time: return
    statistics[stat] = value

def statistic_max(stat:str, value:float):
    ''' updates statistic to max(old value, new value) '''
    if clock() < statistics_start_time: return
    if stat in statistics:
        statistics[stat] = max(statistics[stat], value)
    else:
        statistics[stat] = value

def statistics_compile():
    ''' returns all system statistics '''
    stat = {}
    for s,val in statistics.items():
        if '.avg' in s:
            stat[s] = val / clock()
        else:
            stat[s] = val
    return stat